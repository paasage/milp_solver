import AssemblyKeys._

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
  {
    case "plugin.xml"            => MergeStrategy.discard
    case "plugin.properties"     => MergeStrategy.concat
    case "META-INF/ECLIPSE_.RSA" | "META-INF/ECLIPSEF.RSA" | "META-INF/eclipse.inf" => MergeStrategy.discard
    case ".api_description"      => MergeStrategy.discard
    case ".options"              => MergeStrategy.discard
    case ".DS_Store"             => MergeStrategy.discard
    case "schema/dynamic_package.exsd"  => MergeStrategy.discard
    case "schema/generated_package.exsd"  => MergeStrategy.discard
    case x                       => old(x)
  }
}

artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)